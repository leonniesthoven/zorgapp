package adsd.app.zorgapp;

import java.util.ArrayList;
import java.util.List;

public class ProfileList {

    private ArrayList<Profile> myCollection;


    public ProfileList(){
        this.myCollection = new ArrayList<>();
    }

    public void add(Profile profile){
        myCollection.add(profile);
    }

    public Profile get(int index){
        return myCollection.get(index);
    }



    public void remove(int index){
        myCollection.remove(index);
    }

    public List<Profile> getAllProfiles(){
        return myCollection;
    }


}



