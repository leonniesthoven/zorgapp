package adsd.app.zorgapp;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;


public class ZorgApp {


    private ProfileList profileList = new ProfileList();
    private JList<Profile> profileJlist;
    FileEditor fileEditor = new FileEditor();

    DefaultListModel<Profile> profileModelList;


    private MedicijnList medicijnList = new MedicijnList();
    public JList<Medicijn> medicijnJlist;
    public JList<Medicijn> medicijnJlistVanPatient;
    DefaultListModel<Medicijn> medicijnModelList;

    DefaultListModel<GewichtMeetPunt> gewichtMeetPuntModelList;

    private JButton jButtonEdit;


    public ZorgApp() throws IOException{
        Profile profile = new Profile("Leon", "Nieshtoven", 23, 101, 1.97, 1, "");
        Profile profile2 = new Profile("Sjon", "Albatron", 34, 120, 1.93, 2, "");
        Profile profile3 = new Profile("Karel", "Klein", 80, 120, 1.20, 3, "");

        Medicijn medicijn1 = new Medicijn();
        medicijn1.setMedicijnNaam("Paracetamol");
        medicijn1.setDosering("3");
        medicijn1.setOmschrijving("Tegen hoofdpijn en andere klachten");
        medicijn1.setSoort("Pijnstiller");

        profile.getMyMedicijnen().add(medicijn1);
        profile2.getMyMedicijnen().add(medicijn1);


        Medicijn medicijn2 = new Medicijn();
        medicijn2.setMedicijnNaam("Sanias Lactulose Stroop");
        medicijn2.setDosering("Nvt");
        medicijn2.setOmschrijving("Voor de verstopte medelander");
        medicijn2.setSoort("Laxeermiddel");

        profile.getMyMedicijnen().add(medicijn2);
        profile2.getMyMedicijnen().add(medicijn2);

        Medicijn medicijn3 = new Medicijn();
        medicijn3.setMedicijnNaam("Hooikoorts pil");
        medicijn3.setDosering("18 pillen per uur");
        medicijn3.setOmschrijving("Helpt mensen met een poller allergie");
        medicijn3.setSoort("Anti Allergie");

        profile.getMyMedicijnen().add(medicijn3);
        profile3.getMyMedicijnen().add(medicijn3);

//        System.out.println(medicijnList);
//        System.out.println(profile.getMyMedicijnen());

//        System.out.println(profile.getBmi());
        addProfileToProfileList(profile);
        addProfileToProfileList(profile2);
        addProfileToProfileList(profile3);
        addMedicijnToMedicijnList(medicijn1);
        addMedicijnToMedicijnList(medicijn2);
        addMedicijnToMedicijnList(medicijn3);

        fillMedicijnJList();
        fillMedicijnJListVanPatient(profile);
        fillPatientJList();

        GUI();

    }

    //UI Code
    public JFrame GUI(){
        JFrame frame = new JFrame();

        JButton bMedewerker = new JButton("Inloggen als Medewerker");
        bMedewerker.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
                pressedMedewerkerButton();
            }
        });

        JButton bPatient = new JButton("Inloggen als Patient");
        bPatient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                frame.setVisible(false);
                //              pressedPatientButton();
                selectGUI();

            }
        });

        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));
        panel.setLayout(new GridLayout(0, 1));
        panel.add(bMedewerker);
        panel.add(bPatient);

        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("ZorgApp");
        frame.pack();
        frame.setVisible(true);
        return frame;
    }


    public void getGewichtGrafiek(){

    }

    public JFrame selectGUI(){
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));
        panel.setLayout(new GridLayout(0, 1));

        //Add labels and text boxes for all the medicine values.
        JLabel Patient = new JLabel();
        Patient.setText("");

        panel.add(Patient);
        fillPatientJListAVG();

        panel.add(profileJlist);

        frame.add(panel, BorderLayout.CENTER);
        frame.setTitle("ZorgApp Patient Medicijnen");
        frame.pack();
        frame.setVisible(true);

        profileJlist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt){
                if (evt.getClickCount() == 2) {
//                    System.out.println("Specifiek");
                    loggedInGUI(true, profileJlist.getSelectedValue());
                    compareGewichtList(profileJlist.getSelectedValue());
                    frame.setVisible(false);

                }
            }
        });
        return frame;
    }

    public JFrame GUIPatientMedicijn(Profile profile){
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));
        panel.setLayout(new GridLayout(0, 1));
        //Add labels and text boxes for all the medicine values.
        JLabel jLabel = new JLabel();
        JLabel jLabelSoort = new JLabel();
        JLabel jLabelOmschrijving = new JLabel();
        JLabel jLabelDosering = new JLabel();
        jLabelSoort.setText("Soort: ");
        jLabelOmschrijving.setText("Omschrijving: ");
        jLabelDosering.setText("Dosering: ");

        JTextField fieldSoort = new JTextField();
        JTextField fieldOmschrijving = new JTextField();
        JTextField fieldDosering = new JTextField();

        fieldSoort.setText("");
        fieldOmschrijving.setText("");
        fieldDosering.setText("");

        panel.add(jLabelSoort);
        panel.add(fieldSoort);
        panel.add(jLabelOmschrijving);
        panel.add(fieldOmschrijving);
        panel.add(jLabelDosering);
        panel.add(fieldDosering);

        fillMedicijnJListVanPatient(profile);
        panel.add(medicijnJlist);
        addListenerToList2(fieldOmschrijving, fieldDosering, fieldSoort);

        frame.add(panel, BorderLayout.CENTER);
        frame.setTitle("ZorgApp Patient Medicijnen");
        frame.pack();
        frame.setVisible(true);
        return frame;


    }

    public JFrame loggedInGUI(boolean isPatient, Profile profile){
        JFrame frame = new JFrame();
        frame.setPreferredSize(new Dimension(800, 600));
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));
        panel.setLayout(new GridLayout(0, 1));

        //Add labels and text boxes for all the profile values.
        JLabel jLabelVoornaam = new JLabel();
        JLabel jLabelAchternaam = new JLabel();
        JLabel jLabelLeeftijd = new JLabel();
        JLabel jLabelGewicht = new JLabel();
        jLabelVoornaam.setText("Voornaam: ");
        jLabelAchternaam.setText("Achternaam: ");
        jLabelLeeftijd.setText("Leeftijd: ");
        jLabelGewicht.setText("Gewicht: ");

        JTextField fieldVoornaam = new JTextField();
        JTextField fieldAchternaam = new JTextField();
        JTextField fieldLeeftijd = new JTextField();
        JTextField fieldGewicht = new JTextField();

        fieldVoornaam.setText("Please select a profile to get a value.");
        fieldAchternaam.setText("Please select a profile to get a value.");
        fieldLeeftijd.setText("Please select a profile to get a value.");
        fieldGewicht.setText("Please select a profile to get a value.");

        JButton jButtonRemove = new JButton();
        jButtonRemove.setEnabled(false);
        jButtonRemove.setText("Remove from list");
        JButton jButtonGraph = new JButton();
        jButtonGraph.setEnabled(false);
        jButtonGraph.setText("Show Graph");
        JButton jButtonEdit = new JButton();
        jButtonEdit.setText("Save changes");
        jButtonEdit.setEnabled(false);

        if (isPatient == true) {
            JLabel label = new JLabel("Patient");
            panel.add(label);
        }
        if (isPatient == false) {
            JLabel label = new JLabel("Medewerker");
            panel.add(label);
        }

        panel.add(jLabelVoornaam);
        panel.add(fieldVoornaam);
        panel.add(jLabelAchternaam);
        panel.add(fieldAchternaam);
        panel.add(jLabelLeeftijd);
        panel.add(fieldLeeftijd);
        panel.add(jLabelGewicht);
        panel.add(fieldGewicht);

        panel.add(jButtonRemove);
        panel.add(jButtonEdit);
        panel.add(jButtonGraph);

        profileJlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        if (profile.getVoorNaam() == null) {
            panel.add(profileJlist);
        }
        if (profile.getVoorNaam() != null) {
//            System.out.println("med");
            profileJlist.removeAll();


            for (Profile thisProfile : profileList.getAllProfiles()) {
                if (profile.getVoorNaam() == thisProfile.getVoorNaam() && profile.getAchterNaam() == thisProfile.getAchterNaam()) {
                    fillMyPatientJList(thisProfile);
//                    System.out.println(profileJlist);
                    panel.add(profileJlist);
                }
            }

        }

        jButtonRemove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent){
                profileList.remove(profileJlist.getSelectedIndex());
                panel.remove(profileJlist);
                panel.revalidate();
                panel.repaint();
                fillPatientJList();
                panel.add(profileJlist);
                addListenerToList(fieldVoornaam, fieldAchternaam, fieldLeeftijd, fieldGewicht, jButtonEdit, jButtonGraph);
            }
        });
        jButtonGraph.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent){
                DrawGraph drawGraph = new DrawGraph(getGewichtandDataByProfile(profileJlist.getSelectedValue()));
                drawGraph.createAndShowGui();
            }
        });

        profileJlist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt){
                if (evt.getClickCount() == 2) {
//                    System.out.println("test");
                    GUIPatientMedicijn(profileJlist.getSelectedValue());
                    compareGewichtList(profileJlist.getSelectedValue());
                    getGewichtandDataByProfile(profileJlist.getSelectedValue());


                }
            }
        });
        jButtonEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent){
//                System.out.println(profileJlist.getSelectedIndex());
//                System.out.println(fieldAchternaam.getText());

                profileJlist.getSelectedValue().setWeegdatum(LocalDate.now().toString());

                Profile profile = new Profile(
                        fieldVoornaam.getText(),
                        fieldAchternaam.getText(),
                        Integer.parseInt(fieldLeeftijd.getText()),
                        Integer.parseInt(fieldGewicht.getText()),
                        //Omdat ik een double aangeef wij parseInt een int verwacht komt er een rach op regel 302.
                        profileJlist.getSelectedValue().getLengte(),
                        profileJlist.getSelectedValue().getId(),
                        profileJlist.getSelectedValue().getWeegdatum()

                );
                replaceProfileData(profileJlist.getSelectedIndex(), profile);
                try {
                    fileEditor.AddNewWeight(profile.getId(), profile.getGewicht(), profile.getWeegdatum());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                panel.revalidate();
                panel.repaint();
                panel.remove(profileJlist);
                if (isPatient) {
                    fillMyPatientJList(profile);
                }
                if (isPatient == false) {
                    fillPatientJList();
                }
                panel.add(profileJlist);
                addListenerToList(fieldVoornaam, fieldAchternaam, fieldLeeftijd, fieldGewicht, jButtonEdit, jButtonGraph);
                jButtonEdit.setEnabled(false);
            }
        });

        addListenerToList(fieldVoornaam, fieldAchternaam, fieldLeeftijd, fieldGewicht, jButtonEdit, jButtonGraph);
        frame.add(panel, BorderLayout.CENTER);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("ZorgApp Logged In");
        frame.pack();
        frame.setVisible(true);
        return frame;
    }

    public void addProfileToProfileList(Profile profile){
        profileList.add(profile);
    }

    public void addMedicijnToMedicijnList(Medicijn medicijn){
        medicijnList.add(medicijn);
    }

    public void pressedMedewerkerButton(){
        loggedInGUI(false, new Profile());
    }

    public void addListenerToList(JTextField voorNaam, JTextField achterNaam, JTextField leeftijd, JTextField gewicht, JButton jButtonEdit, JButton jButtonGraph){

        profileJlist.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e){
                if (!e.getValueIsAdjusting()) {
                    voorNaam.setText(profileJlist.getSelectedValue().getVoorNaam());
                    achterNaam.setText(profileJlist.getSelectedValue().getAchterNaam());
                    leeftijd.setText(String.valueOf(profileJlist.getSelectedValue().getLeeftijd()));
                    gewicht.setText(String.valueOf(profileJlist.getSelectedValue().getGewicht()));

                    jButtonEdit.setEnabled(true);
                    jButtonGraph.setEnabled(true);
                }
            }
        });
    }

    public void addListenerToList2(JTextField omschrijving, JTextField dosering, JTextField soort){
        medicijnJlist.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e){
                if (!e.getValueIsAdjusting()) {
                    omschrijving.setText(medicijnJlist.getSelectedValue().getOmschrijving());
                    dosering.setText(medicijnJlist.getSelectedValue().getDosering());
                    soort.setText(medicijnJlist.getSelectedValue().getSoort());
                }
            }
        });
    }

    public void replaceProfileData(int index, Profile profile){
        profileList.getAllProfiles().set(index, profile);
    }

    public void fillMedicijnJList(){
        medicijnModelList = new DefaultListModel<>();
        int index = 0;
        for (Medicijn thisMedicijn : medicijnList.getAllMedicijnen()) {
            medicijnModelList.add(index, thisMedicijn);
            index++;

            //index++ = Index = Index+1;
        }
        medicijnJlist = new JList<>(medicijnModelList);
    }

    public ArrayList<Integer> compareGewichtList(Profile profile){
        gewichtMeetPuntModelList = new DefaultListModel<>();
        int index = 0;
        ArrayList<Integer> gewichtspunten = new ArrayList<>();

        try {
            File myObj = new File("gewichten.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                int iend = data.indexOf(",");
                String patientID;
                String patientDatum;
                if (iend != -1) {
                    patientID = data.substring(0, iend);
                    String patientCompare1 = patientID;
                    String patientCompare2 = Integer.toString(profile.getId());
                    if (patientCompare2.equals(patientCompare1)) {
                        String weightOfPatient = data.replaceAll(patientID + ", ", "");
                        int minusPlace = weightOfPatient.indexOf("!");
                        String correctWeightOfPatient = weightOfPatient.substring(0, minusPlace);

                        System.out.println(correctWeightOfPatient);
                        int weightOfPatientInt = Integer.valueOf(correctWeightOfPatient);
                        gewichtspunten.add(weightOfPatientInt);
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        return gewichtspunten;
    }

    public ArrayList<GewichtMeetPunt> getGewichtandDataByProfile(Profile profile){
        int index = 0;
        ArrayList<GewichtMeetPunt> gewichtMeetPunten = new ArrayList<>();

        try {
            File myObj = new File("gewichten.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                int iend = data.indexOf(",");
                int iend2 = data.indexOf("!");
                String patientID;
                String patientGewicht;
                if (iend != -1) {
                    patientID = data.substring(0, iend);
                    patientGewicht = data.substring(iend + 2, iend2);
                    String patientCompare1 = patientID;
                    String patientCompare2 = Integer.toString(profile.getId());
                    if (patientCompare2.equals(patientCompare1)) {
                        String datum = data.replaceAll(patientID + ", " + patientGewicht, "");
                        String correctDatum = datum.substring(2);

                        System.out.println(correctDatum);
                        GewichtMeetPunt gewichtMeetPunt = new GewichtMeetPunt(correctDatum, patientGewicht);
                        gewichtMeetPunten.add(gewichtMeetPunt);
                    }
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        return gewichtMeetPunten;

    }

//    public void fillMyMedicijnJList(){
//        medicijnModelList = new DefaultListModel<>();
//        int index = 0;
//        for (Medicijn thisMedicijn : medicijnList.getMyMedicijnen()) {
//            medicijnModelList.add(index, thisMedicijn);
//            index++;
//
//            //index++ = Index = Index+1;
//        }
//        medicijnJlist = new JList<>(medicijnModelList);
//    }

    public void fillMedicijnJListVanPatient(Profile profile){
        medicijnModelList = new DefaultListModel<>();
        int index = 0;
        for (Medicijn thisMedicijn : profile.getMyMedicijnen()) {
            medicijnModelList.add(index, thisMedicijn);
            index++;
        }
        medicijnJlist = new JList<>(medicijnModelList);
    }

    public void fillPatientJList(){
        profileModelList = new DefaultListModel<>();
        int index = 0;
        for (Profile thisProfile : profileList.getAllProfiles()) {
            profileModelList.add(index, thisProfile);
            index++;
        }
        profileJlist = new JList<>(profileModelList);
    }

    public void fillMyPatientJList(Profile profile){
        profileModelList = new DefaultListModel<>();
        profileModelList.add(0, profile);
        profileJlist = new JList<>(profileModelList);
    }

    public void fillPatientJListAVG(){
        profileModelList = new DefaultListModel<>();
        int index = 0;
        for (Profile thisProfile : profileList.getAllProfiles()) {
            profileModelList.add(index, new Profile(thisProfile.getVoorNaam(), thisProfile.getAchterNaam()));
            index++;

            //index++ = Index = Index+1;
        }
        profileJlist = new JList<>(profileModelList);
    }

}
