package adsd.app.zorgapp;

import javax.swing.*;
import java.io.IOException;

public class Program {

    public static void main(String[] args) throws IOException{


        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    ZorgApp zorgApp = new ZorgApp();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


    }

}
