package adsd.app.zorgapp;

import java.util.ArrayList;

public class Profile{
    //variables
    private String voorNaam;
    private String achterNaam;
    private int leeftijd;
    private int gewicht;
    private double lengte;

    private String weegdatum;

    private int id;

    private ArrayList<GewichtMeetPunt> gewichtMeetPuntData;

    private ArrayList<Medicijn> myMedicijnen;

    //constructors
    public Profile(){
        myMedicijnen = new ArrayList<Medicijn>();
    }

    public Profile(String voorNaam, String achterNaam){
        this.voorNaam = voorNaam;
        this.achterNaam = achterNaam;
        myMedicijnen = new ArrayList<Medicijn>();
    }

    public Profile(String voorNaam, String achterNaam, int leeftijd, int gewicht, double lengte, int id){
        this.voorNaam = voorNaam;
        this.achterNaam = achterNaam;
        this.leeftijd = leeftijd;
        this.gewicht = gewicht;
        this.lengte = lengte;
        this.id = id;
        myMedicijnen = new ArrayList<Medicijn>();
    }

    public Profile(String voorNaam, String achterNaam, int leeftijd, int gewicht, double lengte, int id, String weegdatum){
        this.voorNaam = voorNaam;
        this.achterNaam = achterNaam;
        this.leeftijd = leeftijd;
        this.gewicht = gewicht;
        this.lengte = lengte;
        this.id = id;
        this.weegdatum = weegdatum;
        myMedicijnen = new ArrayList<Medicijn>();
    }




    //getters & setters
    public String getVoorNaam(){
        return voorNaam;
    }

    public void setVoorNaam(String voorNaam){
        this.voorNaam = voorNaam;
    }

    public String getAchterNaam(){
        return achterNaam;
    }

    public void setAchterNaam(String achterNaam){
        this.achterNaam = achterNaam;
    }

    public int getLeeftijd(){
        return leeftijd;
    }

    public String getBmi(){
        return String.valueOf(gewicht / (lengte * lengte));
    }

    public void setLeeftijd(int leeftijd){
        this.leeftijd = leeftijd;
    }

    public int getGewicht(){
        return gewicht;
    }

//    public void setGewicht(double gewicht){
//        this.gewicht = gewicht;
//    }

    public String getWeegdatum(){
        return weegdatum;
    }

    public void setWeegdatum(String weegdatum){
        this.weegdatum = weegdatum;
    }

    public double getLengte(){
        return lengte;
    }

//    public void setLengte(double lengte){
//        this.lengte = lengte;
//    }


    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public ArrayList<Medicijn> getMyMedicijnen(){
        return myMedicijnen;
    }

    public ArrayList<GewichtMeetPunt> getMyGewichtspunten(){
        return gewichtMeetPuntData;
    }

    @Override
    public String toString(){
        if(gewicht == 0){
            return
                    "  Voornaam:  " + voorNaam +
                    "  Achternaam:  " + achterNaam;
        }
        return
                "  Voornaam:  " + voorNaam +
                "  Achternaam:  " + achterNaam +
                "  Leeftijd:  " + leeftijd +
                "  Gewicht:  " + gewicht + " kg" +
                "  Lengte:  " + lengte + " cm"
                ;
    }

}
