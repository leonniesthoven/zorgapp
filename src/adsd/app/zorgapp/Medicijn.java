package adsd.app.zorgapp;

public class Medicijn{

    private String medicijnNaam;
    private String omschrijving;
    private String soort;
    private String dosering;

    public Medicijn(){

    }

    public String getMedicijnNaam(){
        return medicijnNaam;
    }

    public void setMedicijnNaam(String medicijnNaam){
        this.medicijnNaam = medicijnNaam;
    }

    public String getOmschrijving(){
        return omschrijving;
    }

    public void setOmschrijving(String omschrijving){
        this.omschrijving = omschrijving;
    }

    public String getSoort(){
        return soort;
    }

    public void setSoort(String soort){
        this.soort = soort;
    }

    public String getDosering(){
        return dosering;
    }

    public void setDosering(String dosering){
        this.dosering = dosering;
    }

    @Override
    public String toString(){
        return medicijnNaam;
    }
}
