package adsd.app.zorgapp;

import java.io.IOException;

public interface iEditWeight {

    public void AddNewWeight(int profileID, int gewicht, String datum) throws IOException;
    public void ReadWeightsOfPatient(Profile profile);

}
