package adsd.app.zorgapp;

import java.util.ArrayList;
import java.util.List;

public class MedicijnList {

    private ArrayList<Medicijn> myCollection;


    public MedicijnList(){
        this.myCollection = new ArrayList<>();
    }

    public void add(Medicijn medicijn){
        myCollection.add(medicijn);
    }

    public Medicijn get(int index){
        return myCollection.get(index);
    }



    public void remove(int index){
        myCollection.remove(index);
    }

    public List<Medicijn> getAllMedicijnen(){
        return myCollection;
    }

//    public List<Medicijn> getMyMedicijnen(){
//       return myCollection;
//
//    }


}
