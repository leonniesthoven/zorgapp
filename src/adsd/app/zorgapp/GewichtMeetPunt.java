package adsd.app.zorgapp;

public class GewichtMeetPunt{

    private String datum;
    private String gewicht;

    public GewichtMeetPunt(String datum, String gewicht){
        this.datum = datum;
        this.gewicht = gewicht;
    }

    public String getDatum(){
        return datum;
    }

    public String getGewicht(){
        return gewicht;
    }

}
